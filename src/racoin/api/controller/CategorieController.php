<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 08/12/15
 * Time: 21:54
 */

namespace racoin\api\controller;


use \racoin\common\model\Categorie;

class CategorieController extends AbstractController
{
    public function getAllCategories()
    {
        $router = $this->app->getContainer()->get('router');

        $categorie = Categorie::select('id', 'libelle')->orderBy('libelle', 'asc')->get();

        foreach ($categorie as $cat) {
            $res[] = ['categorie' => $cat, 'links' => ['self' => ['href' => $router->pathFor('categorie', ['id' => $cat->id])]]];
        }

        $tab = array('Categories' => $res, 'Links' => []);

        $encoded = json_encode($tab);

        $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
        $response = $this->Status($response, 200);
        $response = $this->Write($response, $encoded);

        return $response;
    }

    public function getCategorieById($id)
    {
        $router = $this->app->getContainer()->get('router');
        $categorie = Categorie::find($id);

        if(empty($categorie)){
            $res = ['codeErreur' => 404,
                'messageErreur' => "La ressource demandée n'a pas été trouvée",
                'ressourceDemandee' => $router->pathFor('categorie', ['id' => $id])];
            $encoded = json_encode($res);

            //Ecriture du header
            $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
            $response = $this->Status($response, 404);
            $response = $this->Write($response, $encoded);

            return $response;
        }

        $res = ['categorie' => $categorie, 'links' => ['annonce' => ['href' => $router->pathFor('IdCategorie', ['id' => $id])]]];
        $encoded = json_encode($res);

        $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
        $response = $this->Status($response, 200);
        $response = $this->Write($response, $encoded);

        return $response;
    }

    public function getCategorieByAnnounce($id)
    {
        $router = $this->app->getContainer()->get('router');
        $categorie = Categorie::join('annonce', 'categorie.id', '=', 'annonce.cat_id')
            ->select('categorie.id', 'categorie.libelle', 'categorie.descriptif')
            ->where('annonce.id', '=', $id)
            ->get();
        $res = ['categorie' => $categorie, 'links' => ['annonce' => ['href' => $router->pathFor('IdCategorie', ['id' => $categorie[0]->id])]]];
        $encoded = json_encode($res);

        $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
        $response = $this->Status($response, 200);
        $response = $this->Write($response, $encoded);
        return $response;
    }
}