<?php

namespace racoin\api\controller;

use \racoin\common\model\Annonce;
use racoin\common\model\Categorie;

class AnnonceController extends AbstractController
{
    public function getAllAnnonces()
    {
        $router = $this->app->getContainer()->get('router');

        $query = Annonce::select('id', 'titre', 'prix', 'date_online')
            ->where('status', '=', 2)
            ->orderBy('date_online', 'desc');

        if (isset($_GET['min'])) {
            $query->where('prix', '>=', $_GET['min']);
        }
        if (isset($_GET['max'])) {
            $query->where('prix', '<=', $_GET['max']);
        }

        $annonce = $query->get();

        foreach ($annonce as $a) {
            $res[] = ['annonce' => $a, 'links' => ['self' => ['href' => $router->pathFor('annonce', ['id' => $a->id]),
                'annonceur' => $router->pathFor('annonceur', ['id' => $a->id])]]];
        }

        $tab = array('Annonces' => $res, 'Links' => []);

        $encoded = json_encode($tab);

        $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
        $response = $this->Status($response, 200);
        $response = $this->Write($response, $encoded);

        return $response;
    }

    public function createAnnonce()
    {
        $router = $this->app->getContainer()->get('router');

        $cat_id = Categorie::select('*')
            ->where('id', '=', $this->request->getParam('cat_id'))
            ->count();

        if ($cat_id == 0) {
            $tab = array('messageErreur' => 'La catégorie est inexistante.',
                'ressourceDemandée' => $router->pathFor('categorie', ['id' => $this->request->getParam('cat_id')]));

            $data = json_encode($tab);

            $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
            $response = $this->Status($response, 400);
            $response = $this->Write($response, $data);
            return $response;
        } else {
            $annonce = new Annonce();
            $annonce->titre = $this->request->getParam('titre');
            $annonce->descriptif = $this->request->getParam('descriptif');
            $annonce->ville = $this->request->getParam('ville');
            $annonce->code_postal = $this->request->getParam('codepostal');
            $annonce->prix = $this->request->getParam('prix');
            $annonce->passwd = $this->request->getParam('password');
            $annonce->cat_id = $this->request->getParam('cat_id');

            $annonce->save();

            $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
            $response = $this->jsonHeader($response, 'Content-Location', $router->pathFor('annonce', ['id' => $annonce->id]));
            $response = $this->Status($response, 201);
            $response = $this->Write($response, '');
            return $response;
        }
    }

    public function getAnnonceById($id)
    {
        $router = $this->app->getContainer()->get('router');

        $annonce = Annonce::select('id', 'titre', 'prix', 'date_online')
            ->where('id', '=', $id)
            ->where('status', '=', 2)
            ->get();

        if (empty($annonce[0])) {

            $res = ['codeErreur' => 404,
                'messageErreur' => "La ressource demandée n'a pas été trouvée",
                'ressourceDemandee' => $router->pathFor('annonce', ['id' => $id])];
            $encoded = json_encode($res);

            //Ecriture du header
            $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
            $response = $this->Status($response, 404);
            $response = $this->Write($response, $encoded);

            return $response;
        }

        $res = ['annonce' => $annonce, 'links' => ['categorie' => ['href' => $router->pathFor('categorieAnnonce', ['id' => $id])],
            'annonceur' => ['href' => $router->pathFor('annonceur', ['id' => $id])]]];
        $encoded = json_encode($res);

        //Ecriture du header
        $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
        $response = $this->Status($response, 200);
        $response = $this->Write($response, $encoded);

        return $response;
    }

    public function getAnnouncesByCategorie($id)
    {
        $router = $this->app->getContainer()->get('router');

        $query = Annonce::select('id', 'titre', 'prix', 'date_online')
            ->where('cat_id', '=', $id)
            ->where('status', '=', 2);

        if (isset($_GET['min'])) {
            $query->where('prix', '>=', $_GET['min']);
        }
        if (isset($_GET['max'])) {
            $query->where('prix', '<=', $_GET['max']);
        }

        $annonce = $query->get();

        foreach ($annonce as $a) {
            $res[] = ['annonce' => $a, 'links' => ['self' => ['href' => $router->pathFor('annonce', ['id' => $a->id])]]];
        }

        $tab = array('Annonces' => $res, 'Links' => []);

        $encoded = json_encode($tab);

        $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
        $response = $this->Status($response, 200);
        $response = $this->Write($response, $encoded);
        return $response;
    }

    public function deleteAnnounce($id)
    {
        $annonce = Annonce::find($id);
        $annonce->status = 0;
        $annonce->save();
    }

    public function createAnnounceInCategorie($id)
    {
        $router = $this->app->getContainer()->get('router');

        $categorie = Categorie::select('*')->where('id', '=', $id)->count();

        if ($categorie == 0) {
            $tab = array('messageErreur' => 'La catégorie est inexistante.',
                'ressourceDemandée' => $router->pathFor('categorie', ['id' => $id]));

            $data = json_encode($tab);

            $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
            $response = $this->Status($response, 400);
            $response = $this->Write($response, $data);
            return $response;
        } else {
            $annonce = new Annonce();
            $annonce->titre = $this->request->getParam('titre');
            $annonce->descriptif = $this->request->getParam('descriptif');
            $annonce->ville = $this->request->getParam('ville');
            $annonce->code_postal = $this->request->getParam('codepostal');
            $annonce->prix = $this->request->getParam('prix');
            $annonce->passwd = $this->request->getParam('password');
            $annonce->cat_id = $id;

            $annonce->save();

            $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
            $response = $this->jsonHeader($response, 'Content-Location', $router->pathFor('annonce', ['id' => $annonce->id]));
            $response = $this->Status($response, 201);
            $response = $this->Write($response, '');
            return $response;
        }
    }

    public function getAnnonceur($id)
    {
        $router = $this->app->getContainer()->get('router');

        $annonceur = Annonce::select('ville', 'code_postal', 'nom_a', 'prenom_a', 'mail_a', 'tel_a')
            ->where('id', '=', $id)
            ->get();

        $res = ['annonceur' => $annonceur, 'links' => ['annonces' => ['href' => $router
            ->pathFor('annoncesAnnonceur', ['mail' => $annonceur[0]->mail_a])]]];

        $encoded = json_encode($res);

        //Ecriture du header
        $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
        $response = $this->Status($response, 200);
        $response = $this->Write($response, $encoded);

        return $response;
    }

    public function createAnnonceur()
    {
        $annonceur = new Annonce();

        $annonceur->nom_a = $this->request->getParams('nom');
        $annonceur->prenom_a = $this->request->getParams('prenom');
        $annonceur->mail_a = $this->request->getParams('mail');
        $annonceur->tel_a = $this->request->getParams('tel');

        $annonceur->save();
    }

    public function getAnnonceByAnnonceur($mail)
    {
        $router = $this->app->getContainer()->get('router');

        $annonces = Annonce::select('id', 'titre', 'prix', 'date_online')
            ->where('mail_a', '=', $mail)
            ->where('status', '=', 2)
            ->get();

        foreach ($annonces as $a) {
            $res[] = ['annonce' => $a, 'links' => ['annonces' => ['href' => $router
                ->pathFor('annonce', ['id' => $a->id])]]];
        }

        $tab = array('Annonces' => $res, 'Links' => []);

        $encoded = json_encode($tab);

        //Ecriture du header
        $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
        $response = $this->Status($response, 200);
        $response = $this->Write($response, $encoded);

        return $response;
    }
}