<?php

namespace racoin\api\controller;

use racoin\common\model\Apikey;

class ApikeyController extends AbstractController
{
    public function verifyApiKey($key)
    {
        $apikey = Apikey::select('apikey')->where('apikey', '=', $key)->get();

        if (!empty($apikey[0]))
            $verif = true;

        else $verif = false;

        return $verif;
    }

    public function apiCount($key){
        $idkey = Apikey::select('id')->where('apikey', '=', $key)->get();

        $apikey = Apikey::find($idkey[0]->id);
        $apikey->count += 1;

        $apikey->save();
    }
}