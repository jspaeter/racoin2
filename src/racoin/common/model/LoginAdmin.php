<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 12/02/16
 * Time: 14:46
 */

namespace racoin\common\model;
use Illuminate\Database\Eloquent\Model;

class LoginAdmin extends Model
{
    protected $table = 'login_admin';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
