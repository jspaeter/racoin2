<?php

namespace racoin\common\model;

class Categorie extends \Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $table = 'categorie';
    protected $primaryKey = 'Id';
}