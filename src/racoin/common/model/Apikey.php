<?php

namespace racoin\common\model;

use Illuminate\Database\Eloquent\Model;


class Apikey extends Model
{
    protected $table = 'key';
    protected $primaryKey = 'id';
    public $timestamps = false;
}