<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 12/02/16
 * Time: 14:37
 */

namespace racoin\backend\controller;

use racoin\common\model\LoginAdmin;

class LoginAdminController extends AbstractController
{
    public function checkLogin($usr, $pwd)
    {
        $user = htmlspecialchars($usr);
        $password = htmlspecialchars($pwd);

        $admin = LoginAdmin::select('user_pwd')->where('user_login', '=', $user)->first();

        if($admin){
            if (password_verify($password, $admin->user_pwd)) {
                return true;
            }
        }
        else {
            return false;
        }
    }
}