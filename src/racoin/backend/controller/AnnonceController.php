<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 19/01/16
 * Time: 16:27
 */

namespace racoin\backend\controller;

use \racoin\common\model\Annonce;

class AnnonceController extends AbstractController
{
    public function getUnvalidatedAnnounces()
    {
        $router = $this->app->getContainer()->get('router');
        $template = $this->app->getContainer()->get('twig')->loadTemplate('unvalidated_announce.html');
        $route['back'] = $router->pathFor('home');

        $annonces = Annonce::select('id', 'titre', 'prix', 'date_online')->where('status', '=', 1)->get();

        foreach ($annonces as $a) {
            $a->router = $router->pathFor('announceById', ['id' => $a->id]);
        }
        return $template->render(array('annonces' => $annonces, 'route' => $route));
    }

    public function getAnnounceById($id)
    {
        $router = $this->app->getContainer()->get('router');
        $annonce = Annonce::select('id', 'titre', 'prix', 'date_online', 'ville', 'descriptif', 'mail_a', 'tel_a')
            ->where('status', '=', 1)
            ->where('id', '=', $id)
            ->get();

        if (!empty($annonce[0])) {
            $template = $this->app->getContainer()->get('twig')->loadTemplate('announce.html');
            $route['back'] = $router->pathFor('unvalidated');
            $route['validate'] = $router->pathFor('validateAnnounce', ['id' => $annonce[0]->id]);
            $route['delete'] = $router->pathFor('deleteAnnounce', ['id' => $annonce[0]->id]);
            $route['next'] = $router->pathFor('nextAnnounce', ['id' => $annonce[0]->id]);
            return $template->render(array('annonce' => $annonce, 'route' => $route));
        } else {
            $template = $this->app->getContainer()->get('twig')->loadTemplate('error.html');
            $tab = ['msgType' => 'error',
                    'titre' => 'Erreur',
                    'message' => 'Annonce inexistante ou non valide.'];
            $route['back'] = $router->pathFor('unvalidated');
            return $template->render(array('error' => $tab, 'route' => $route));
        }
    }

    public function validateAnnounceById($id)
    {
        $router = $this->app->getContainer()->get('router');
        $annonce = Annonce::find($id);

        if (!empty($annonce[0])) {
            $template = $this->app->getContainer()->get('twig')->loadTemplate('error.html');
            $annonce->status = 2;
            $annonce->save();

            $tab = ['msgType' => 'validation','message' => 'Annonce validée !'];
            $route['back'] = $router->pathFor('unvalidated');
            return $template->render(array('error' => $tab, 'route' => $route));
        } else {
            $template = $this->app->getContainer()->get('twig')->loadTemplate('error.html');
            $tab = ['msgType' => 'error',
                'titre' => 'Erreur',
                'message' => 'Annonce inexistante ou non valide.'];
            $route['back'] = $router->pathFor('unvalidated');
            return $template->render(array('error' => $tab, 'route' => $route));
        }
    }

    public function deleteAnnounceById($id)
    {
        $router = $this->app->getContainer()->get('router');
        $annonce = Annonce::find($id);

        if (!empty($annonce[0])) {
            $template = $this->app->getContainer()->get('twig')->loadTemplate('error.html');
            $annonce->status = 3;
            $annonce->save();
            $tab = ['msgType' => 'validation', 'message' => 'Annonce supprimée !'];
            $route['back'] = $router->pathFor('unvalidated');
            return $template->render(array('error' => $tab, 'route' => $route));
        } else {
            $template = $this->app->getContainer()->get('twig')->loadTemplate('error.html');
            $tab = ['msgType' => 'error',
                    'titre' => 'Erreur',
                    'message' => 'Annonce inexistante ou non valide.'];
            $route['back'] = $router->pathFor('unvalidated');
            return $template->render(array('error' => $tab, 'route' => $route));
        }
    }

    public function getNextAnnounce($id)
    {
        $router = $this->app->getContainer()->get('router');
        $annonce = Annonce::select('id', 'titre', 'prix', 'date_online', 'ville', 'descriptif', 'mail_a', 'tel_a')
            ->where('id', '>', $id)
            ->where('status', '=', 1)
            ->take(1)
            ->get();

        if (!empty($annonce[0])) {
            $template = $this->app->getContainer()->get('twig')->loadTemplate('announce.html');
            $route['back'] = $router->pathFor('unvalidated');
            $route['validate'] = $router->pathFor('validateAnnounce', ['id' => $annonce[0]->id]);
            $route['delete'] = $router->pathFor('deleteAnnounce', ['id' => $annonce[0]->id]);
            $route['next'] = $router->pathFor('nextAnnounce', ['id' => $annonce[0]->id]);
            return $template->render(array('annonce' => $annonce, 'route' => $route));
        } else {
            $template = $this->app->getContainer()->get('twig')->loadTemplate('error.html');
            $tab = ['msgType' => 'error',
                    'titre' => 'Erreur',
                    'message' => 'Annonce inexistante ou non valide.'];
            $route['back'] = $router->pathFor('unvalidated');
            return $template->render(array('error' => $tab, 'route' => $route));
        }
    }
}