<?php

session_start();
if (!isset($_SESSION["logged"])) $_SESSION["logged"] = 0;

require_once '../vendor/autoload.php';
use racoin\backend\controller as Controller;

$container = new Slim\Container();

$container['flash'] = function ($c) {
    return new \Slim\Flash\Messages();
};

$container['twig'] = function ($container) {
    $loader = new Twig_Loader_Filesystem('web/view');
    $twig = new Twig_Environment($loader, array('debug' => true));

    $twig->addGlobal("session", $_SESSION);
//    $twig->addGlobal("flash", $container['flash']);

    return $twig;
};

racoin\app\App::DbConf('../src/racoin/utils/config.ini');

$app = new \Slim\App($container);

//Slim3 error handler
$c = $app->getContainer();
$c['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        $data = [
            'code' => $exception->getCode(),
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'trace' => explode("\n", $exception->getTraceAsString()),
        ];

        return $c->get('response')->withStatus(500)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($data));
    };
};


//On check si tout est OK
function checkLogin()
{
    if (!isset($_SESSION['logged']) || !isset($_SESSION['ip']) ||
        $_SESSION['logged'] != 1 || $_SESSION['ip'] != $_SERVER['REMOTE_ADDR']
    ) {
        return false;
    } else {
        return true;
    }
}

//Toutes les actions pour le backend
$app->group('/', function () use ($app) {

    //Post pour s'authentifier
    $app->post('', function ($req, $res) use ($app) {

        //Récupération des données POST
        $postData = $req->getParsedBody();
        $controller = new Controller\LoginAdminController($app);
        $logged = $controller->checkLogin($postData['username'], $postData['password']);


        //Si on a réussi à s'authentifier
        if ($logged) {
            //On crée une session logged et une session avec l'IP de la personne
            unset($_SESSION['slimFlash']);
            $_SESSION['logged'] = 1;
            $_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
            session_regenerate_id();

            $template = $app->getContainer()->get('twig')->loadTemplate('login.html');
            return $template->render(array('logged' => true));
        } //Sinon on renvoie un message d'erreur
        else {
            $this->flash->addMessage('LoginFailed', "Nom d'utilisateur ou mot de passe incorrect.");
            return $res->withStatus(302)->withHeader('Location', '../backend');
        }
    });

    //Affichage de la page de login
    $app->get('', function () use ($app) {

        $template = $app->getContainer()->get('twig')->loadTemplate('login.html');
        return $template->render(array());
    })->setName('home');

    //Redirection si /logout
    $app->get('logout', function ($req, $res) use ($app) {

        session_destroy();
        return $res->withStatus(301)->withHeader('Location', '../backend');
    })->setName('logout');

    //Affichage de la liste des annonces non validées
    $app->get('unvalidated', function ($req, $res) use ($app) {

        if (checkLogin()) {
            $controller = new Controller\AnnonceController($app);
            return $controller->getUnvalidatedAnnounces();
        } else {
            $this->flash->addMessage('AccessNotAllowed', "Vous n'avez pas le droit d'accéder à cette page.");
            return $res->withStatus(302)->withHeader('Location', '../backend');
        }
    })->setName('unvalidated');

    //Récupération d'une annonce
    $app->get('annonce/{id}', function ($req, $res, $args) use ($app) {

        if (checkLogin()) {
            $id = $args['id'];
            $this->flash->addMessage('next', false);
            $controller = new Controller\AnnonceController($app);
            return $controller->getAnnounceById($id);
        } else {
            $this->flash->addMessage('AccessNotAllowed', "Vous n'avez pas le droit d'accéder à cette page.");
            return $res->withStatus(302)->withHeader('Location', '../');
        }
    })->setName('announceById');

    //Validation d'une annonce
    $app->get('annonce/{id}/validate', function ($req, $res, $args) use ($app) {

        if (checkLogin()) {
            $id = $args['id'];
            $controller = new Controller\AnnonceController($app);
            return $controller->validateAnnounceById($id);
        } else {
            $this->flash->addMessage('AccessNotAllowed', "Vous n'avez pas le droit d'accéder à cette page.");
            return $res->withStatus(302)->withHeader('Location', '../');
        }
    })->setName('validateAnnounce');

    //Suppression d'une annonce
    $app->get('annonce/{id}/delete', function ($req, $res, $args) use ($app) {

        if (checkLogin()) {
            $id = $args['id'];
            $controller = new Controller\AnnonceController($app);

            return $controller->deleteAnnounceById($id);
        } else {
            $this->flash->addMessage('AccessNotAllowed', "Vous n'avez pas le droit d'accéder à cette page.");
            return $res->withStatus(302)->withHeader('Location', '../');
        }
    })->setName('deleteAnnounce');

    //Récupération de l'annonce suivante
    $app->get('annonce/{id}/next', function ($req, $res, $args) use ($app) {

        if (checkLogin()) {
            $id = $args['id'];
            $controller = new Controller\AnnonceController($app);
            $this->flash->addMessage('next', true);
            return $controller->getNextAnnounce($id);
        } else {
            $this->flash->addMessage('AccessNotAllowed', "Vous n'avez pas le droit d'accéder à cette page.");
            return $res->withStatus(302)->withHeader('Location', '../');
        }
    })->setName('nextAnnounce');
});

$app->run();