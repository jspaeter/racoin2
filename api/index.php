<?php

require_once '../vendor/autoload.php';

racoin\app\App::DbConf('../src/racoin/utils/config.ini');

use \racoin\api\controller as Controller;

$app = new \Slim\App();

//Création Middleware
$app->add(function ($req, $res, $next) use ($app) {
    $controller = new Controller\ApikeyController($req, $res, $app);
    if (!empty($req->getQueryParams()['key'])) {
        $key = $controller->verifyApiKey($req->getQueryParams()['key']);

        if ($key) {
            $res = $next($req, $res);
            if ($res->getStatusCode() != 404)
                $controller->apiCount($req->getQueryParams()['key']);
        } else {
            $mes = ['Message' => 'Clé api invalide'];
            $encoded = json_encode($mes);

            $response = $res->withHeader('Content-Type', 'application/json')->withStatus(403)->write($encoded);

            return $response;
        }
    } else {
        $mes = ['Message' => "Veuillez entrer une clé d'api valide"];
        $encoded = json_encode($mes);

        $response = $res->withHeader('Content-Type', 'application/json')->withStatus(403)->write($encoded);

        return $response;
    }
    return $res;
});

//Toutes les actions pour 'catégories'
$app->group('/categories', function () use ($app) {

    ///////////// Retourne la liste des catégories /////////////
    $app->get('', function ($req, $res) use ($app) {

        $controller = new Controller\CategorieController($req, $res, $app);
        return $controller->getAllCategories();
    });

    ///////////// Retourne la catégorie spécifiée /////////////
    $app->get('/{id}', function ($req, $res, $args) use ($app) {

        $id = $args['id'];
        $controller = new Controller\CategorieController($req, $res, $app);
        return $controller->getCategorieById($id);
    })->setName('categorie');

    $app->get('/{id}/annonces', function ($req, $res, $args) use ($app) {

        $id = $args['id'];
        $controller = new Controller\AnnonceController($req, $res, $app);
        return $controller->getAnnouncesByCategorie($id);
    })->setName('IdCategorie');

    $app->post('/{id}/annonce', function ($req, $res, $args) use ($app) {
        $id = $args['id'];
        $controller = new Controller\AnnonceController($req, $res, $app);
        return $controller->createAnnounceInCategorie($id);
    });
});

//Toutes les actions pour 'annonces'
$app->group('/annonces', function () use ($app) {

    //Get sur '/annonces'
    ///////////// Retourne la liste des annonces /////////////
    $app->get('', function ($req, $res, $app) use ($app) {

        $annonces = new Controller\AnnonceController($req, $res, $app);
        return $annonces->getAllAnnonces();
    });

    //Get sur '/annonces/id'
    ///////////// Retourne l'annonce spécifiée /////////////
    $app->get('/{id}', function ($req, $res, $args) use ($app) {

        $id = $args['id'];
        $controller = new Controller\AnnonceController($req, $res, $app);
        return $controller->getAnnonceById($id);
    })->setName('annonce');

    //Get sur '/annonces/id/categorie'
    ///////////// Retourne la catégorie de l'annonce spécifiée /////////////
    $app->get('/{id}/categorie', function ($req, $res, $args) use ($app) {

        $id = $args['id'];
        $controller = new Controller\CategorieController($req, $res, $app);
        return $controller->getCategorieByAnnounce($id);
    })->setName('categorieAnnonce');


    $app->delete('/{id}', function ($req, $res, $args) use ($app) {

        $id = $args['id'];
        $controller = new Controller\AnnonceController($req, $res, $app);
        return $controller->deleteAnnounce($id);
    });

    //Get sur '/annonces/id/annonceur'
    ///////////// Retourne les coordonnées de l'annonceur sur l'annonce spécifiée /////////////
    $app->get('/{id}/annonceur', function ($req, $res, $args) use ($app) {

        $id = $args['id'];
        $controller = new Controller\AnnonceController($req, $res, $app);
        return $controller->getAnnonceur($id);
    })->setName('annonceur');

    //Post sur '/annonces'
    ///////////// Création d'une annonce /////////////
    $app->post('', function ($req, $res) use ($app) {

        $createdannounce = new Controller\AnnonceController($req, $res, $app);
        return $createdannounce->createAnnonce();
    });
});

$app->get('/annonceur/{mail}/annonces', function ($req, $res, $args) use ($app) {

    $mail = $args['mail'];

    $controller = new Controller\AnnonceController($req, $res, $app);
    return $controller->getAnnonceByAnnonceur($mail);
})->setName('annoncesAnnonceur');

$app->run();